{ pkgs ? import <nixpkgs> {}
}:

pkgs.stdenv.mkDerivation rec {
  name = "my-xmonad";

  nativeBuildInputs = with pkgs; [
    cabal-install
    ghc
    pkgconfig
    xorg.libX11.dev
    xorg.libXft
    xorg.libXScrnSaver
    xorg.libXext
    xorg.libXinerama
    xorg.libXrender
    xorg.libXrandr
  ];
}
